package Tidal_Project
  import Modelica.Units.SI;




  annotation(
    Icon(graphics = {Rectangle(lineColor = {0, 170, 255}, fillColor = {85, 85, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(origin = {-3.61125, 4.14368}, points = {{-85.9993, -11.9788}, {-57.9993, 16.0212}, {-25.9993, -13.9788}, {4.00067, 12.0212}, {30.0007, -15.9788}, {64.0007, 12.0212}, {86.0007, -11.9788}}, smooth = Smooth.Bezier), Line(origin = {-3.61125, 4.14368}, points = {{-85.9993, -11.9788}, {-57.9993, 16.0212}, {-25.9993, -13.9788}, {4.00067, 12.0212}, {30.0007, -15.9788}, {64.0007, 12.0212}, {86.0007, -11.9788}}, smooth = Smooth.Bezier), Line(origin = {0.148343, 40.1284}, points = {{-85.9993, -11.9788}, {-57.9993, 16.0212}, {-25.9993, -13.9788}, {4.00067, 12.0212}, {30.0007, -15.9788}, {64.0007, 12.0212}, {86.0007, -11.9788}}, smooth = Smooth.Bezier)}));
end Tidal_Project;
