within Tidal_Project;

model Mer
  parameter Real h0(unit = "m") = 7.1 "hauteur initiale de la mer";
  parameter Real A(unit = "m") = 4.8 "Amplitude de la marrée"; 
  parameter Real f(unit = "/s") = 2.25e-5 "fréqence de la marrée";
  Interfaces.SignalPort S_Mer annotation(
    Placement(visible = true, transformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.FluidPort turbines annotation(
    Placement(visible = true, transformation(origin = {100, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.FluidPort vannes annotation(
    Placement(visible = true, transformation(origin = {100, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation

  turbines.h = h0 + A*sin(time);
  S_Mer.s=turbines.h;
  vannes.h=turbines.h;
  
annotation(
    Icon(graphics = {Polygon(origin = {24, 51}, lineColor = {255, 85, 0}, fillColor = {255, 85, 0}, fillPattern = FillPattern.Solid, points = {{-10, -5}, {28, -33}, {-26, -33}, {-30, -7}, {-10, -5}}, smooth = Smooth.Bezier), Rectangle(lineColor = {0, 170, 255}, fillColor = {0, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Polygon(origin = {-32, 1}, lineColor = {255, 85, 0}, fillColor = {255, 85, 0}, fillPattern = FillPattern.Solid, points = {{-26, 33}, {26, -3}, {-26, -33}, {-26, 33}, {-26, 33}}), Ellipse(origin = {24, -2}, lineColor = {255, 85, 0}, fillColor = {255, 85, 0}, fillPattern = FillPattern.Solid, extent = {{-42, 34}, {42, -34}}), Polygon(origin = {24, 51}, lineColor = {255, 85, 0}, fillColor = {255, 85, 0}, fillPattern = FillPattern.Solid, points = {{-10, -5}, {28, -33}, {-26, -33}, {-30, -7}, {-10, -5}}, smooth = Smooth.Bezier)}));
annotation(
    Icon(graphics = {Rectangle(lineColor = {0, 170, 255}, fillColor = {0, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Ellipse(origin = {18, -2}, lineColor = {255, 85, 0}, fillColor = {255, 85, 0}, fillPattern = FillPattern.Solid, extent = {{-34, 32}, {34, -32}}), Polygon(origin = {-28, -4}, lineColor = {255, 85, 0}, fillColor = {255, 85, 0}, fillPattern = FillPattern.Solid, points = {{2, -8}, {-30, 36}, {50, -6}, {-22, -40}, {2, -8}}, smooth = Smooth.Bezier), Polygon(origin = {14, 44}, lineColor = {255, 85, 0}, fillColor = {255, 85, 0}, fillPattern = FillPattern.Solid, points = {{-30, 0}, {10, -8}, {28, -26}, {-14, -48}, {-30, 0}}, smooth = Smooth.Bezier)}));
end Mer;

//2*2*Modelica.Math.asin(1.0)*f*
