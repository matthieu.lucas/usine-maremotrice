within Tidal_Project.Composants;

model Vannes
  Modelica.Units.SI.Length H "drop height";
  Tidal_Project.Interfaces.FluidPort mer annotation(
    Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

  parameter Real alpha_d=4.49419e-4 "sens direct";
  parameter Real alpha_i=5.71715e-4 "sens indirect";
  parameter Real beta_d=1.51079e-2 "sens direct";
  parameter Real beta_i=3.37798e-2 "sens indirect";
  Interfaces.SignalPort S_Vannes annotation(
    Placement(visible = true, transformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal_Project.Interfaces.FluidPort bassin annotation(
    Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  bassin.Qv = mer.Qv;
//conservation du flux
  H = bassin.h - mer.h;
//hauteur de chute
  if integer(S_Vannes.s) == 1 then
    if H > 0 then
      mer.Qv = 6*4 * H / beta_d;
    else
      mer.Qv = 6*4 * H / beta_i;
    end if;
  else
    mer.Qv = 0;
  end if;
  annotation(
    Icon(graphics = {Rectangle(fillColor = {73, 164, 249}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-100, 100}, {100, -100}}), Rectangle(origin = {0, -30}, fillColor = {109, 109, 109}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-100, 70}, {100, -70}}), Rectangle(origin = {-44, -54}, lineColor = {0, 0, 255}, fillColor = {0, 85, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-16, 46}, {16, -46}}), Rectangle(origin = {44, -54}, lineColor = {0, 0, 255}, fillColor = {0, 85, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-16, 46}, {16, -46}}), Rectangle(origin = {-44, 0}, lineColor = {0, 0, 255}, fillColor = {77, 77, 77}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-16, 20}, {16, -20}}), Rectangle(origin = {44, 0}, lineColor = {0, 0, 255}, fillColor = {77, 77, 77}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-16, 20}, {16, -20}})}));
end Vannes;
