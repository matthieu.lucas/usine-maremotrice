within Tidal_Project.Composants;

model Command
  Interfaces.SignalPort S_Bassin annotation(
    Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.SignalPort S_Vannes annotation(
    Placement(visible = true, transformation(origin = {40, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {40, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.SignalPort S_Turbines annotation(
    Placement(visible = true, transformation(origin = {-40, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-40, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.SignalPort S_Mer annotation(
    Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  if integer(S_Turbines.s)==0 then
    if S_Bassin.s-S_Mer.s>=5 then
      S_Turbines.s=2;
      S_Vannes.s=0;
    elseif S_Bassin.s-S_Mer.s<=0.1 then
      S_Turbines.s=1;
      S_Vannes.s=1;
    else
      S_Turbines.s=0;
      S_Vannes.s=0;
    end if;
  elseif integer(S_Turbines.s)==1 then
    if S_Bassin.s>=10 then
      S_Turbines.s=3;
      S_Vannes.s=0;
    else
      S_Turbines.s=1;
      S_Vannes.s=1;
    end if;
  elseif integer(S_Turbines.s)==2 then
    if S_Bassin.s-S_Mer.s<=0.5 then
      S_Turbines.s=0;
      S_Vannes.s=0;
    else
      S_Turbines.s=2;
      S_Vannes.s=0;
    end if;
  else
    if S_Bassin.s-S_Mer.s>=3 then
      S_Turbines.s=0;
      S_Vannes.s=0;
    else
      S_Turbines.s=3;
      S_Vannes.s=0;
    end if;
  end if;
      

annotation(
    Diagram);
end Command;
